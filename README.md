### Demo Project: ###
Use Docker for local development
### Technologies used: ###
Docker, Node.js, MongoDB, MongoExpress
### Project Description: ###
- Create Dockerfile for Nodejs application and build Docker image
- Run Nodejs application in Docker container and connect to MongoDB database container locally. 
- Also run MongoExpress container as a UI of the MongoDB database.

### AWS ECR Repo - Private Docker Repository
Pre-Requisites:
- AWS CLI is installed
- Credentials configured

1. Create ecr repository on AWS
2. Push image to the ecr repo

Build docker image from Dockerfile
- docker build -t my-app:1.0 .
- docker tag imageName:tag repo_address/imageName:tag
- docker push repo_address/imageName:tag
- docker-compose -f mongo.yaml up

### Docker Volumes
Mounts host file system to container virtual file system using docker-compose:
- docker run -v /home/mount/data(host file system):/var/lib/mysql/data(container file system) - Host volume 
- docker run -v /var/lib/mysql/data(container file system) - Anonymous Volume
- docker run -v named:/var/lib/mysql/data(container file system) - named Volume (should use)


To access the shell of the Docker VM in order to view volume information, use this command:
 docker run -it --privileged --pid=host debian nsenter -t 1 -m -u -n -i sh


### Create Docker Hosted repo on Nexus
- Create user role for Docker repo
- Assign role to the Nexus user
- Configure HTTP and Port for Docker repo
- Configure Port on Droplet firewall
- Activate Docker Bearer token in Realms on Nexus

### Configure Docker Desktop to allow insecure regiestries ###
 Settings -> Docker Engine - "insecure-registries": ["nexus_REPO_address: PORT"]

### Nexus Repo - Private Docker Repository
- docker login nexus_REPO_address:PORT
- build docker image
- tag image docker tag imageName:tag nexus_REPO_address:PORT/imageName:tag
- docker push nexus_REPO_address:PORT/imageName:tag

### Fetch Docker images from Nexus ###
 - curl -u username:pwd -X 'http://nexus_REPO_address:PORTservice/rest/v1/components?repository=repoName'

### Deploy Nexus as Docker Container
- Create a Droplet - ssh to it
- Install Docker on the droplet
- Configure Volume - follow Nexus image doc on Docker Hub
    - docker volume create --name nexus-data
- docker run -d -p --name nexus -v nexus-data:/nexus-data sonatype/nexus3
- docker volume ls
- docker inspect nexus-data - use it to find admin.password


## demo app - developing with Docker

This demo app shows a simple user profile app set up using 
- index.html with pure js and css styles
- nodejs backend with express module
- mongodb for data storage

All components are docker-based

### With Docker

#### To start the application

Step 1: Create docker network

    docker network create mongo-network 

Step 2: start mongodb 

    docker run -d -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME=admin -e MONGO_INITDB_ROOT_PASSWORD=password --name mongodb --net mongo-network mongo    

Step 3: start mongo-express
    
    docker run -d -p 8081:8081 -e ME_CONFIG_MONGODB_ADMINUSERNAME=admin -e ME_CONFIG_MONGODB_ADMINPASSWORD=password --net mongo-network --name mongo-express -e ME_CONFIG_MONGODB_SERVER=mongodb mongo-express   

_NOTE: creating docker-network in optional. You can start both containers in a default network. In this case, just emit `--net` flag in `docker run` command_

Step 4: open mongo-express from browser

    http://localhost:8081

Step 5: create `user-account` _db_ and `users` _collection_ in mongo-express

Step 6: Start your nodejs application locally - go to `app` directory of project 

    cd app
    npm install 
    node server.js
    
Step 7: Access you nodejs application UI from browser

    http://localhost:3000

### With Docker Compose

#### To start the application

Step 1: start mongodb and mongo-express

    docker-compose -f docker-compose.yaml up
    
_You can access the mongo-express under localhost:8080 from your browser_
    
Step 2: in mongo-express UI - create a new database "my-db"

Step 3: in mongo-express UI - create a new collection "users" in the database "my-db"       
    
Step 4: start node server 

    cd app
    npm install
    node server.js
    
Step 5: access the nodejs application from browser 

    http://localhost:3000

#### To build a docker image from the application

    docker build -t my-app:1.0 .       
    
The dot "." at the end of the command denotes location of the Dockerfile.

##### Module 7: Containers with Docker #####